#include <Adafruit_MotorShield.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

Adafruit_DCMotor *R_front_motor = AFMS.getMotor(4);
Adafruit_DCMotor *R_back_motor = AFMS.getMotor(3);
Adafruit_DCMotor *L_front_motor = AFMS.getMotor(1);
Adafruit_DCMotor *L_back_motor = AFMS.getMotor(2);

char buf[80];

String FORWARD_COMMAND = "FORWARD";
String ROTATE_CW_COMMAND = "ROTATE_CW";
String ROTATE_CCW_COMMAND = "ROTATE_CCW";
String STOP_COMMAND = "STOP";
String REVERSE_COMMAND = "REVERSE";
String SET_SPEED_COMMAND = "SET_SPEED";
String SET_DRIVE_SPEED_COMMAND = "SET_DRIVE_SPEED";
String SET_ROTATION_SPEED_COMMAND = "SET_ROTATION_SPEED";

int DEFAULT_DRIVE_POWER = 180;
int DEFAULT_ROTATION_POWER = 215;
int DRIVE_OFFSET_R = 0;
float POWER_CONSTANT = 1;

int drive_power = DEFAULT_DRIVE_POWER * POWER_CONSTANT; 
int rotation_power = DEFAULT_ROTATION_POWER * POWER_CONSTANT;


// REGION - Motor control

void rotate_cw() {
  set_motor_power(rotation_power); 
  L_front_motor -> run(FORWARD);
  L_back_motor -> run(FORWARD);
  R_front_motor -> run(BACKWARD);
  R_back_motor -> run(BACKWARD);
}

void rotate_ccw() {
  set_motor_power(rotation_power);
  L_front_motor -> run(BACKWARD);
  L_back_motor -> run(BACKWARD);
  R_front_motor -> run(FORWARD);
  R_back_motor -> run(FORWARD);
}

void stop_() {
  L_front_motor -> run(RELEASE);
  L_back_motor -> run(RELEASE);
  R_front_motor -> run(RELEASE);
  R_back_motor -> run(RELEASE);
}

void forward() {
  stop_();
  set_motor_power(drive_power);
  L_front_motor -> run(FORWARD);
  L_back_motor -> run(FORWARD);
  R_front_motor -> run(FORWARD);
  R_back_motor -> run(FORWARD);
}

void reverse() {
  stop_();
  set_motor_power(drive_power);
  L_front_motor -> run(BACKWARD);
  L_back_motor -> run(BACKWARD);
  R_front_motor -> run(BACKWARD);
  R_back_motor -> run(BACKWARD);
}

void set_motor_power(int power) {
  L_front_motor -> setSpeed(power);
  L_back_motor -> setSpeed(power);
  R_front_motor -> setSpeed(power);
  R_back_motor -> setSpeed(power);
}

boolean set_motor_speed(int speed_) {
  if (speed_ > 255 || speed_ < 0) return false;
  set_motor_power(speed_);
  return true;
}

boolean set_speed_variable(int speed_, int* speed_variable) {
  if (speed_ > 255 || speed_ < 0) return false;
  *speed_variable = speed_;
  set_motor_power(speed_);
  return true;
}

// END REGION

// REGION - Input parsing

int read_line(int readch, char *buffer, int len) {
  static int pos = 0;
  int rpos;
  
  if (readch > 0) {
    switch (readch) {
      case '\r': // Ignore CR
        break;
      case '\n': // Return on new-line
        rpos = pos;
        pos = 0;  // Reset position index ready for next time
        return rpos;
      default:
      if (pos < len-1) {
        buffer[pos++] = readch;
        buffer[pos] = 0;
        }
    }
  }
  return 0;
}

String get_input() {
  if (read_line(Serial.read(), buf, 80) > 0) {
    String input = buf;
    input.toUpperCase();
    input.trim();
    return carry_out_action(input);
  }

  return "";
}

long get_speed_val() {
  while (!(read_line(Serial.read(), buf, 80) > 0)) {
  }
  return atoi(buf);
}

boolean is_set_speed_command(String input) {
  return (input.length() > SET_SPEED_COMMAND.length() && input.substring(0, SET_SPEED_COMMAND.length()) == SET_SPEED_COMMAND);
}

boolean is_set_drive_speed_command(String input) {
  return (input.length() > SET_DRIVE_SPEED_COMMAND.length() 
  && input.substring(0, SET_DRIVE_SPEED_COMMAND.length()) == SET_DRIVE_SPEED_COMMAND);
}

boolean is_set_rotation_speed_command(String input) {
  return (input.length() > SET_ROTATION_SPEED_COMMAND.length() 
  && (input.substring(0, SET_ROTATION_SPEED_COMMAND.length()) == SET_ROTATION_SPEED_COMMAND));
}

String carry_out_action(String input) {
  if (input == FORWARD_COMMAND) {
    forward();
  }
  else if (input == REVERSE_COMMAND) {
    reverse();
  }
  else if (input == STOP_COMMAND) {
    stop_();
  }
  else if (input == ROTATE_CW_COMMAND) {
    rotate_cw();
  }
  else if (input == ROTATE_CCW_COMMAND) {
    rotate_ccw();
  }
  else if (is_set_speed_command(input)) {
    set_motor_speed(input.substring(SET_SPEED_COMMAND.length() + 1).toInt());
  }
  else if (is_set_drive_speed_command(input)) {
    set_speed_variable(input.substring(SET_DRIVE_SPEED_COMMAND.length() + 1).toInt(), &drive_power);
  } 
  else if (is_set_rotation_speed_command(input)) {
    set_speed_variable(input.substring(SET_ROTATION_SPEED_COMMAND.length() + 1).toInt(), &rotation_power);
  }
  else {
    return "INVALID";
  }
  return "SUCCESS";
}

// END REGION

void setup() {
  AFMS.begin();
  Serial.begin(115200);

  // Turns motors on
  L_front_motor -> run(RELEASE);
  L_back_motor -> run(RELEASE);
  R_front_motor -> run(RELEASE);
  R_back_motor -> run(RELEASE);
}

void loop() {
  String result = get_input();
  if (result != "") Serial.println(result);
}
