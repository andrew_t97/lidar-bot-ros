# LiDAR Bot ROS package
This repo contains the outcome of a project to create a basic autonomous mapping solution using a small 3D printed UGV equipped with a LiDAR. The project acted as a learning exercise for me to gain further knowledge in ROS, system design, modelling in Fusion 360, and 3D printing.
 
When running the ros package the robot will traverse the environment while mapping using the RPLIDAR A2 to measure distances. If it encounters an obstacle the bot will stop and rotate to a random angle in the general opposite direction. Every 30 seconds a map will be saved allowing you to view its current state. It's also possible to view a live progression of the map in RVIZ however the Raspberry Pi 4 struggles to run both rviz and the ros package so I do not recommend doing this unless you upgrade the RPi to something more powerful.
 
## The robot
 
![alt-text](readme_resources/robot.jpg)
 
## Hardware Requirements
The following is the hardware I used in the project, some of this can be easily swapped out like the Arduino and Raspberry Pi for something more powerful/practical.
 
- RPLIDAR A2
- Arduino Uno
- Arduino Motorshield V2
- Raspberry Pi 4
- Raspberry Pi Fan
- 4x Micro metal gear motor
- Ni-MH 9.6v 1800mAh battery pack
- [5000mAh powerbank](https://www.amazon.co.uk/gp/product/B07KY63Z3R/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)
- Tamiya Male-Female connector
- [2x Lego Tire & Tread with 36 Treads Large, Non-Technic](https://www.bricklink.com/v2/catalog/catalogitem.page?P=x1681#T=S&O={%22iconly%22:0})
- 2x M4 20mm bolts
- 10x M3 14mm bolts
- 2x M4 lock nuts
 
## Software Requirements
- Python 2
- ROS Melodic
- Arduino IDE
- [Hector SLAM ROS package](http://wiki.ros.org/hector_slam)
- [RPLIDAR ROS package](http://wiki.ros.org/rplidar)
 
## Software Installation
 
1. First clone the git repository into your ROS workspace
   ```bash
   git clone https://gitlab.com/andrew_t97/lidar-bot-ros.git
   ```
2. Clone Hector SLAM and RPLIDAR into your ROS workspace
3. Run `catkin_make`. Make sure you're using Python 2 otherwise you'll encounter issues with a geometry package in ros
4. Source your ROS setup file
5. Flash motor_control.ino to your Arduino Uno
 
## Running the package
Once you have everything plugged in and installed you can now run the package. In order to do this run the following command:
 
```bash
roslaunch lidar_bot basic.launch
```
This will then start the mapping and the robot as well as the node used to save maps periodically. The robot uses several different nodes to achieve collision detection, collision handling, and movement. These nodes can be easily re-purposed independently due to the nature of the ROS architecture.
 
The following is an example of one of the saved maps produced during mapping:
 
![alt-text](readme_resources/example.png)
 
The purple line indicates the path taken by the bot during the mapping process. The yellow arrow indicates the starting point of the bot. The blue pixels indicate obstacles within the room e.g. walls, objects, me.
 
## 3D Printable parts
The chassis was designed from the ground up. It consists of two main parts; the lid and the base - both of which can be 3D printed using PLA. The lid requires support when printed, however the base and PCB platform does not.
 
The wheels used to drive the tracks are also 3D printable and come in two variants. A master wheel which is holed specifically for the motor shaft and a slave wheel which is holed for an M4 bolt. These wheels can be 3D printed with minimum support and are designed to effectively drive the tracks. The slave wheels require a block to hold it in place on the inside of the chassis, this can be 3D printed and placed where a motor would be. You can then thread the bolt through the wheel and mounting block, then secure it using one of the lock nuts.
 
By using a modular wheel system it allows for any combination of motors and slave wheels. In the current design the 4 outer wheels are driven by motors and the central wheels act as slave wheels. If you wish to reduce the drain on the battery and are happy to sacrifice torque I recommend only using a single motor for each track.
 
There is an internal platform which the Arduino and Raspberry Pi 4 are mounted to. The design of the chassis allows this platform to be swapped out with something else which allows for mounting of other PCBs. The only requirement is that the platform fits into the chassis itself.
 
## Future plans
- Create an assembly instruction manual
- Attempt to use SLAM based collision avoidance using a local map
- Implement pathfinding based on the map created using SLAM
- Install an IMU into the system to improve movement tracking
- Replace geared motors with 2 brushless motors
 
## Previous design iterations
### SMARS
The initial inspiration for this project came from the thingiverse [SMARS project](https://www.thingiverse.com/thing:2662828). I started with this system and experimented with its capabilities. The robot is very basic, using only what can be achieved through an arduino and is designed for very little payload due to the use of two small motors and a small 9V battery. The main appeal of this design is that a large amount of it can be 3D printed e.g. chassis, wheels, tracks.
 
The main issue with this design was that it couldn't accommodate any high fidelity sensors due to the size and drive power. However, the design was extremely useful in understanding the basics of motor control with the Arduino as well as modular motor systems.
 
### V1
Version 1 was very similar in nature to the SMARS bot. It consisted of a large number of 3D printable parts, used the same motor base layout and was quite open which helped reduce plastic and printing time.
 
<img src="readme_resources/Version 1.png" alt="Version 1 model" width="600"/>
 
The design accomodated a Raspberry Pi and a power source to run it, as well as the RPLIDAR A2. The design had several issues, the tracks were plastic so on low-friction surfaces such as wood and plastic it would slip reducing the traction greatly and making rotation near impossible. The tracks would also struggle to turn the bot on soft surfaces such as carpet, this was mainly due to the high weight to surface area ratio, as well as the length of the robot making it difficult to rotate.
 
The design was also quite messy once everything was plugged in due to the chassis being open. This meant wires tended to spread just about everywhere including into the tracks. The RPLIDAR A2's power block was also mounted on the outside of the chassis as an afterthought as I believed it could be placed inside with all the wires (it couldn't). The lid also had a tendency to slide off as it was only mounted on rails rather than bolts meaning if the bot accelerated too quickly the lid would jerk back and move the position of the LIDAR in relation to the robot frame.
 
As a result of these issues I decided to carry out a redesign.
 
### V2
Version 2 moved away from the 3D printed tracks as these were one of the main problems in version 1. I purchased the largest rubber lego tracks I could find and built the chassis around the use of these. This resulted in a squarer chassis and required a rework of the internals. I also took the opportunity to build a closed chassis which utilised bolts to secure its independent parts.
 
The design consisted of the base which contained the Arduino, the 9V battery, the power bank for the Raspberry Pi, and the motors. It also contained a platform which the Raspberry Pi was mounted to which then clipped into the inner walls of the chassis. The lid acted as a mount for the RPLIDAR A2 and closed the base to the outside world. The LiDAR's power block was moved inside the chassis in order to help reduce the number of cables outside.
 
<img src="readme_resources/Version 2.png" alt="Version 2 model" width="600"/>
 
This design worked well, however the 9V battery struggled to hold a voltage as it drained rather rapidly with the 4 motors. This meant that I was burning through 9V batteries at a considerable rate and also the robot was unable to carry out much of a mapping before the voltage got so low it struggled to rotate again. As a result of this I purchased a large battery pack and set to re-designing the chassis again to accommodate this.
 
### V3 - Current design
Version 3 was a slightly larger design as it had to accomodate a larger battery. I also took advantage of another iteration of design and introduced some more vents and holes in the chassis to reduce the amount of plastic used in printing and to improve the airflow for the Raspberry Pi. I also introduced a mounting for a small fan which blows directly onto the Pi to reduce the effects of temperature throttling. The Arduino was also moved up to the same platform as the Raspberry Pi in order to allow for the battery pack to fit into position.
 
This design was the most effective so far as it allowed the bot to perform mapping without mechanical/power issues and allowed for a full mapping to occur.
 
<img src="readme_resources/Version 3.png" alt="Version 3 model" width="600"/>