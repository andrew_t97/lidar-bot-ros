#!/usr/bin/env python
import rospy
from lidar_bot.srv import HandleCollision
from lidar_bot.srv import MotorComm, MotorCommRequest, MotorCommResponse
from sensor_msgs.msg import LaserScan

from collision_detection_utils import CollisionDetection

class CollisionDetectionSubscriber:

    def __init__(self):
        rospy.wait_for_service("collision_handler")
        self.collision_handler = rospy.ServiceProxy("collision_handler", HandleCollision)
        rospy.wait_for_service("motor_comms")
        self.motor_comm = rospy.ServiceProxy("motor_comms", MotorComm)

        self.collision_detector = CollisionDetection()
        self.id_count = 0
        self.stopped = True
        
    def motor_comms_client(self, command, speed=100):
        try:
            return self.motor_comm(str(self.id_count), command, speed).status
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed: %s" % e)

    def callback(self, scan):
        if self.collision_detector.is_collision_possible(scan):
            result = self.collision_handler(str(self.id_count), rospy.get_rostime())
            rospy.loginfo("Reached angle!")
            self.stopped = True
        elif self.stopped:
            self.motor_comms_client(MotorCommRequest.SET_DRIVE_SPEED, 140)
            self.motor_comms_client(MotorCommRequest.FORWARD)
            self.stopped = False
        else:
            rospy.logdebug("No collisions detected.")
        
    def listener(self):
        rospy.init_node('collision_detector', anonymous=True)
        rospy.Subscriber('scan', LaserScan, self.callback, queue_size=1)
        rospy.spin()
 
if __name__ == '__main__':
    collision_detector = CollisionDetectionSubscriber()
    collision_detector.listener()
