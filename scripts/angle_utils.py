#!/usr/bin/env python

import rospy
from random import randint
from random import choice
from lidar_bot.srv import MotorComm, MotorCommRequest

def get_random_opposite_angle(angle, range_from_opposite_angle):
    opposite_angle_delta = 180 + randint(0, range_from_opposite_angle)
    if randint(0, 1) == 1:
        new_angle = subtract_angles(angle, opposite_angle_delta)
    else:
        new_angle = add_angles(angle, opposite_angle_delta)

    return new_angle

def is_bot_at_desired_angle(current_angle, desired_angle, acceptable_error_range):
    return is_angle_within_range(current_angle, desired_angle, acceptable_error_range)

def should_bot_decelerate(current_angle, desired_angle, deceleration_angle_range):
    return is_angle_within_range(current_angle, desired_angle, deceleration_angle_range)

def has_bot_overshot(current_angle, desired_angle, direction, acceptable_error_range):
    min_angle = subtract_angles(desired_angle, acceptable_error_range)
    max_angle = add_angles(desired_angle, acceptable_error_range)
    opposite_of_desired_angle = add_angles(desired_angle, 180)

    if direction == MotorCommRequest.ROTATE_CW:
        if max_angle > opposite_of_desired_angle:
            return max_angle < current_angle <= 180 or -180 <= current_angle <= opposite_of_desired_angle
        else:
            return max_angle < current_angle <= opposite_of_desired_angle

    elif direction == MotorCommRequest.ROTATE_CCW:
        if min_angle < opposite_of_desired_angle:
            return -180 <= current_angle < min_angle or opposite_of_desired_angle <= current_angle <= 180
        else:
            return opposite_of_desired_angle <= current_angle < min_angle

    else:
        rospy.logwarn("Invalid MotorComm direction provided to method has_bot_overshot")
        return False

def new_has_bot_overshot(current_angle, target_angle, current_direction):
    proposed_direction = get_direction(current_angle, target_angle)
    return current_direction != proposed_direction

def get_direction(current_angle, desired_angle):
    """Calculates the direction a bot should rotate in in order to reach the desired angle
    in the shortest amount of time

    Args:
        current_angle (int): The current angle of the bot
        desired_angle (int): The angle we want the bot to move to

    Returns:
        int: Returns the values associated with either MotorCommRequest.ROTATE_CW
        or MotorCommRequest.ROTATE_CCW
    """
    angle_difference = abs(current_angle - desired_angle)
    if current_angle < desired_angle:
        if angle_difference < 180:
            return MotorCommRequest.ROTATE_CCW
        else:
            return MotorCommRequest.ROTATE_CW
    else:
        if angle_difference < 180:
            return MotorCommRequest.ROTATE_CW
        else:
            return MotorCommRequest.ROTATE_CCW

def is_angle_within_range(angle, target_angle, angle_range):
    """Checks if a given angle is within a certain range from another angle

    Args:
        angle (int): The angle we want to check if is in the provided range
        target_angle (int): The angle at which we want to add/subtract angle_range from
        angle_range (int): The size of the range we want from our target angle

    Returns:
        boolean: Returns true if angle is within the range [target_angle +/- angle_range]
    """
    min_angle = subtract_angles(target_angle, angle_range)
    max_angle = add_angles(target_angle, angle_range)

    if min_angle > max_angle:
        return min_angle <= angle <= 180 or -180 <= angle <= max_angle
    else:  
        return min_angle <= angle <= max_angle

def add_angles(a, b):
    return normalise_angle(int(a + b))

def subtract_angles(a, b):
    return normalise_angle(int(a - b))

def normalise_angle(angle):
    if angle < -180:
        return angle % 180
    elif angle > 180:
        return angle % -180
    else:
        return angle
