#!/usr/bin/env python
import rospy
import math
import numpy as np
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose

def callback(scan):
    rospy.loginfo("I heard a laser scan %s[%d]:" % (scan.header.frame_id, scan.scan_time/scan.time_increment))
    rospy.loginfo("angle_range, %f, %f" % (math.degrees(scan.angle_min), math.degrees(scan.angle_max)))
    scans = inverse_scan(scan)

    for i in scans:
        rospy.loginfo("Degree: %f, Range: %f" % (i[0], i[1]))
    
def listener():
    rospy.init_node('lidar_listener', anonymous=True)
    rospy.Subscriber('scan', LaserScan, callback)
    rospy.spin()
 
if __name__ == '__main__':
    listener()
