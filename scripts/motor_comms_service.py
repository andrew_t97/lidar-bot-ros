#!/usr/bin/env python
import rospy
from lidar_bot.srv import MotorComm, MotorCommRequest, MotorCommResponse
from bot_control import BotControl

class MotorCommsService:
    def __init__(self):
        self.bot_controller = BotControl()
    
    def callback(self, req):
        result = False

        # Movement controllers
        if req.command == MotorCommRequest.FORWARD:
            result = self.bot_controller.drive_forward()
        elif req.command == MotorCommRequest.BACKWARD:
            result = self.bot_controller.drive_backward()
        elif req.command == MotorCommRequest.ROTATE_CW:
            result = self.bot_controller.rotate_cw()
        elif req.command == MotorCommRequest.ROTATE_CCW:
            result = self.bot_controller.rotate_ccw()
        elif req.command == MotorCommRequest.STOP:
            result = self.bot_controller.stop()
        
        # Speed controllers
        elif req.command == MotorCommRequest.SET_SPEED:
            result = self.bot_controller.set_speed(req.speed)
        elif req.command == MotorCommRequest.SET_DRIVE_SPEED:
            result = self.bot_controller.set_drive_speed(req.speed)
        elif req.command == MotorCommRequest.SET_ROTATION_SPEED:
            result = self.bot_controller.set_rotation_speed(req.speed)
        else:
            rospy.loginfo("MotorComm command not recognised [id: %s, command: %d]" % (req.id, req.command))

        if result:
            return MotorCommResponse(MotorCommResponse.SUCCESS)

        return MotorCommResponse(MotorCommResponse.FAILED)

    def motor_comms_server(self):
        rospy.init_node('motor_comms_server')
        rospy.Service('motor_comms', MotorComm, self.callback)
        rospy.spin()

    def shutdown(self):
        self.bot_controller.cleanup()

if __name__ == "__main__":
    motor_comms = MotorCommsService()
    rospy.on_shutdown(motor_comms.shutdown)
    motor_comms.motor_comms_server()

 
