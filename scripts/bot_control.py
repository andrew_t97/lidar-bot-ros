#!/usr/bin/env python
import serial
import time
import traceback
import logging

class BotControl:
    
    def __init__(self):
        self.ser = serial.Serial('/dev/ttyUSB1', 115200, timeout=0)
        self.ser.flush()
        time.sleep(2)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logging.debug("TYPE: ", exc_type)
        logging.debug("VAL: ", exc_val)
        traceback.print_tb(exc_tb)
        self.cleanup()

    def send_message(self, message):
        if message[-1] != '\n':
            message += '\n'
        
        self.ser.write(message.encode('utf-8')) 
        result = ""
        while result == "":
            result = self.ser.readline().decode('utf-8').rstrip()

        return result == "SUCCESS"

    def cleanup(self):
        self.stop()

    def drive_forward(self):
        return self.send_message("forward\n")

    def drive_backward(self):
        return self.send_message("reverse\n")

    def rotate_cw(self):
        return self.send_message("rotate_cw\n")

    def rotate_ccw(self):
       return self.send_message("rotate_ccw\n")
            
    def stop(self):
        return self.send_message("stop\n")

    def set_speed(self, speed):
        if speed < 0 or speed > 255:
            return False
        return self.send_message(("set_speed " + str(speed) + "\n"))

    def set_drive_speed(self, speed):
        if speed < 0 or speed > 255:
            return False
        return self.send_message("set_drive_speed " + str(speed) + "\n")

    def set_rotation_speed(self, speed):
        if speed < 0 or speed > 255:
            return False
        return self.send_message("set_rotation_speed " + str(speed) + "\n")


if __name__ == '__main__':
    controller = BotControl()
    controller.drive_forward()
    time.sleep(2)
    controller.drive_backward()
    time.sleep(2)
    controller.rotate_cw()
    time.sleep(2)
    controller.rotate_ccw()
    time.sleep(2)
    controller.set_speed(240)
    time.sleep(2)
    controller.drive_forward()
    controller.set_drive_speed(150)
    time.sleep(2)
    controller.set_rotation_speed(150)
    controller.rotate_cw()
    time.sleep(2)
    controller.stop()
