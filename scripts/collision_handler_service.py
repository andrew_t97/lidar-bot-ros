#!/usr/bin/env python
import rospy
import angle_utils

from lidar_bot.srv import HandleCollision, HandleCollisionRequest, HandleCollisionResponse
from lidar_bot.srv import MotorComm, MotorCommRequest, MotorCommResponse
from geometry_msgs.msg import PoseStamped
from tf.transformations import euler_from_quaternion
from math import degrees

class BotRotationController:
    ACCEPTABLE_ANGLE_ERROR = 5
    DECELERATION_ANGLE = 50
    MAX_RANGE_OF_NEW_ANGLE = 50
    SLOW_SPEED = 110
    DEFAULT_SPEED = 120

    def __init__(self, rotation_id):
        self.id_count = 0
        self.target_angle = 0
        self.direction = MotorCommRequest.ROTATE_CW
        self.is_bot_rotating = False
        self.has_bot_reached_angle = False
        self.has_bot_been_slowed = False
        self.rotation_id = rotation_id

        rospy.wait_for_service("motor_comms")
        self.motor_comm = rospy.ServiceProxy("motor_comms", MotorComm)
        self.motor_comms_client(MotorCommRequest.STOP)
        self.subscriber = rospy.Subscriber('slam_out_pose', PoseStamped, self.subscriber_callback)
        
    def subscriber_callback(self, pose_stamped):
        orientation = pose_stamped.pose.orientation
        explicit_quat = [orientation.x, orientation.y, orientation.z, orientation.w]
        roll, pitch, yaw = euler_from_quaternion(explicit_quat)
        yaw = degrees(yaw)
        rospy.logdebug("Current angle deg: %f, Target angle deg: %f" % (yaw, self.target_angle))

        if not self.is_bot_rotating:
            self.initialise_bot_rotation(yaw)
        self.rotate_bot_to_desired_angle(yaw)

    def initialise_bot_rotation(self, current_angle):
        self.target_angle = angle_utils.get_random_opposite_angle(current_angle, self.MAX_RANGE_OF_NEW_ANGLE)
        rospy.loginfo("Target angle deg: %f" % self.target_angle)

        if self.cleanup_if_bot_is_at_target_angle(current_angle):
            return
        
        self.direction = angle_utils.get_direction(current_angle, self.target_angle)
        self.motor_comms_client(MotorCommRequest.SET_ROTATION_SPEED, self.DEFAULT_SPEED)
        self.motor_comms_client(self.direction)
        self.is_bot_rotating = True

    def rotate_bot_to_desired_angle(self, current_angle):
        if self.cleanup_if_bot_is_at_target_angle(current_angle) or self.has_bot_reached_angle:
            return

        if angle_utils.new_has_bot_overshot(current_angle, self.target_angle, self.direction):
            self.motor_comms_client(MotorCommRequest.STOP)
            if self.direction == MotorCommRequest.ROTATE_CW:
                self.direction = MotorCommRequest.ROTATE_CCW          
            else:
                self.direction = MotorCommRequest.ROTATE_CW

            if not self.has_bot_been_slowed:
                self.motor_comms_client(MotorCommRequest.SET_ROTATION_SPEED, self.SLOW_SPEED)
            self.motor_comms_client(self.direction)

        if not self.has_bot_been_slowed and angle_utils.should_bot_decelerate(current_angle, self.target_angle, self.DECELERATION_ANGLE):
            self.motor_comms_client(MotorCommRequest.SET_ROTATION_SPEED, self.SLOW_SPEED)
            self.has_bot_been_slowed = True
            rospy.loginfo("Slowed")

    def motor_comms_client(self, command, speed=100):
        try:
            return self.motor_comm(str(self.rotation_id) + "_" + str(self.get_id_count()), command, speed).status
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed: %s" % e)

    def get_id_count(self):
        temp = self.id_count
        self.id_count += 1
        return temp

    def cleanup_if_bot_is_at_target_angle(self, current_angle):
        if angle_utils.is_bot_at_desired_angle(current_angle, self.target_angle, self.ACCEPTABLE_ANGLE_ERROR):
            self.cleanup()
            return True
        return False

    def cleanup(self):
        self.motor_comms_client(MotorCommRequest.STOP)
        self.has_bot_reached_angle = True
        self.subscriber.unregister()

class HandleCollisionService:
    rotation_id_count = 0

    def service_callback(self, req):
        bot_rotation_controller = BotRotationController(str(self.rotation_id_count))
        r = rospy.Rate(20)
        while not bot_rotation_controller.has_bot_reached_angle:
            r.sleep()

        self.rotation_id_count += 1
        return HandleCollisionResponse(HandleCollisionResponse.SUCCESS)

    def handle_collision_service(self):
        rospy.init_node('collision_handler_server')
        rospy.Service('collision_handler', HandleCollision, self.service_callback)
        rospy.spin()

if __name__ == '__main__':
    collision_handler = HandleCollisionService()
    collision_handler.handle_collision_service()
