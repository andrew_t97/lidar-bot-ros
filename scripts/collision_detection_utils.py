#!/usr/bin/env python
import rospy
import math
import angle_utils
from numpy import arange
from sensor_msgs.msg import LaserScan

class CollisionDetection:
    # Distances in mm
    BOT_WIDTH = 191.0
    LIDAR_DISTANCE_FROM_FRONT = 60.24
    LIDAR_DISTANCE_FROM_SIDE_OF_BOT = BOT_WIDTH/2
    ROBOT_DISTANCE_FROM_WALL = 300.0
    ANGLE_ERROR_BUFFER = 2.0

    def __init__(self, range_origin=180):
        """Constructor

        Args:
            range_origin (int, optional): The center of the range of angles we're checking for collisions at. Defaults to 180.
        """
        self.range_origin = range_origin
        self.collision_angle_range = None
    
    def get_collision_angle_range(self):
        """Calculates the range of angles to check for collisions

        Returns:
            int: The upper/lower limit of the range of angles to check for collisions
        """
        if self.collision_angle_range is None:
            total_distance = self.LIDAR_DISTANCE_FROM_FRONT + self.ROBOT_DISTANCE_FROM_WALL
            self.collision_angle_range = math.degrees(math.atan(self.LIDAR_DISTANCE_FROM_SIDE_OF_BOT / total_distance)) + self.ANGLE_ERROR_BUFFER
        return self.collision_angle_range

    def is_collision_possible(self, scan):
        """Checks if there are any objects in the way of the bot which it could collide with

        Args:
            scan (LaserScan): The current LaserScan object

        Returns:
            boolean: Returns true if the bot will collide with an object
        """
        min_distance = self.LIDAR_DISTANCE_FROM_FRONT + self.ROBOT_DISTANCE_FROM_WALL
        current_angle = math.degrees(scan.angle_min)

        for dist in scan.ranges:
            if angle_utils.is_angle_within_range(current_angle, self.range_origin, self.get_collision_angle_range()) and (dist * 1000) <= min_distance: 
                return True  
            current_angle += math.degrees(scan.angle_increment)
        
        return False